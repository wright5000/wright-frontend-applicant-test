var app = angular.module('app', []);

app.controller('ReportCtrl', function($scope, $http) {

  $scope.data = [];

  $http.get('dist/data.json')
    .then(function(res){

      var deviceTotal = 0;
      var channelTotal = 0;
      
      var arr = [{name: "All", showAll: true}];
      
      $scope.selectedCountry = arr[0];
      
      angular.forEach(res.data, function(value, key){

        var combined = value.Devices + value.Channels;
        chanPercent = parseInt((value.Channels/combined) * 100) ;
        
        arr.push({name: key,
                  metrics: value,
                  showAll: false,
                  total: combined,
                  channelPercent: chanPercent});
        
        deviceTotal += value.Devices;
        channelTotal += value.Channels;
      });
      
      $scope.deviceTotal = deviceTotal;
      $scope.channelTotal = channelTotal;

      arr[0].metrics = {Devices: deviceTotal, Channels: channelTotal};
      $scope.data = arr;

    });

  
});


app.component('bar', {
  templateUrl: 'dist/bar.html',
  bindings: {
    data: "<",
    name: "<",
    devices: "<",
    channels: "<",
  }
});
